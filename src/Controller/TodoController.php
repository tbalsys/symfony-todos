<?php

namespace App\Controller;

use App\Entity\Todo;
use App\Form\TodoType;
use App\Repository\TodoRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TodoController extends AbstractController
{
    /**
     * @Route("/", name="todo_index", methods="GET")
     */
    public function index(TodoRepository $todoRepository, Request $request): Response
    {
        $orderBy = $request->query->get('sort');

        $invertedOrderBy = [];
        foreach (['id', 'title', 'done'] as $column) {
            $invertedOrderBy[$column] = isset($orderBy[$column]) && $orderBy[$column] !== 'desc' ? 'desc' : 'asc';
        }

        return $this->render('todo/index.html.twig', [
            'todos' => $todoRepository->findBy([], $orderBy),
            'order_by' => $invertedOrderBy,
        ]);
    }

    /**
     * @Route("/new", name="todo_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $todo = new Todo();
        $form = $this->createForm(TodoType::class, $todo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($todo);
            $em->flush();

            return $this->redirectToRoute('todo_index');
        }

        return $this->render('todo/new.html.twig', [
            'todo' => $todo,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="todo_show", methods="GET", requirements={"id"="\d+"})
     */
    public function show(Todo $todo): Response
    {
        return $this->render('todo/show.html.twig', ['todo' => $todo]);
    }

    /**
     * @Route("/{id}/edit", name="todo_edit", methods="GET|POST", requirements={"id"="\d+"})
     */
    public function edit(Request $request, Todo $todo): Response
    {
        $form = $this->createForm(TodoType::class, $todo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('todo_edit', ['id' => $todo->getId()]);
        }

        return $this->render('todo/edit.html.twig', [
            'todo' => $todo,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="todo_delete", methods="DELETE", requirements={"id"="\d+"})
     */
    public function delete(Request $request, Todo $todo): Response
    {
        if ($this->isCsrfTokenValid('delete'.$todo->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($todo);
            $em->flush();
        }

        return $this->redirectToRoute('todo_index');
    }
}
