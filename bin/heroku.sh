#!/usr/bin/env bash

heroku create symfony-todos --region eu
heroku addons:create heroku-postgresql:hobby-dev
heroku config:set APP_ENV=prod APP_SECRET=c6096be410dbf6633ff48eb9a7e39dfd MAILER_URL=null://localhost
